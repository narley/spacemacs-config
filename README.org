* Notes for having a working Spacemacs

** For now make sure `spacemacs` is on `develop` branch
     - go to `~/.emacs.d
     - execute `git checkout develop`

** Load `spacemacs` file from this repo
   - replace `.spacemacs` in `~` with `.spacemacs` from this repo.
